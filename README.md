

Source code for WordMaker (1989)- WordProcessor for the Apple Macintosh Computer 
Binaries: Send us them if you have them.
Uploaded to gitlab historical preservation.
Many thanks to Canux Corporation for releasing the source code.

License
=======
WordMaker is distributed under the terms of the GNU General Public License,
version 2 or later. See the COPYING file for details.

All files part of WordMaker has
Copyright (C) 1996-2018 Canux Corporation

Notes
======
This is part of a batch of releases from Canux Corporation.
The source code was obtained from floppys disks, which was found 
and preserved with a kryoflux (thanks SPS).

Please submit any work you might to do to our gitlab in
a pull request.

A bit of the source code history from the copyright owner:

"
All of the sources with the exception of DesignWorks 2.0 and PowerManager
 (and I don’t know if that source was included in what I sent you) were created 
by a US company called “New Horizons”. When NH sold off their IP 
(intellectual property) I was leading a development team at a company
 called WCi in Ottawa Ontario and we bid on and won the auction of those assets.
 I believe that was in 1994. 

We rolled out a new version of DesignWorks (as mentioned) along with other 
products like an Amiga power manager, a keyboard protocol converter (KB-10) and others.

WCi went out of business in 1995 or 1996.  I personally bid on and 
won the auction for WCi’s IP sometime after that. We did not do any 
more Amiga development afterwards but some of our software was featured 
in Amiga magazines at the time and we continued to build and sell our hardware products."


Cheers!

Twitter
https://twitter.com/AmigaSourcePres

Gitlab
https://gitlab.com/AmigaSourcePres

WWW
https://amigasourcepres.gitlab.io/

  _____ ___   _   __  __     _   __  __ ___ ___   _   
 |_   _| __| /_\ |  \/  |   /_\ |  \/  |_ _/ __| /_\  
   | | | _| / _ \| |\/| |  / _ \| |\/| || | (_ |/ _ \ 
  _|_| |___/_/ \_\_|_ |_|_/_/_\_\_|__|_|___\___/_/_\_\
 / __|/ _ \| | | | _ \/ __| __|  / __/ _ \|   \| __|  
 \__ \ (_) | |_| |   / (__| _|  | (_| (_) | |) | _|   
 |___/\___/_\___/|_|_\\___|___|__\___\___/|___/|___|_ 
 | _ \ _ \ __/ __| __| _ \ \ / /_\_   _|_ _/ _ \| \| |
 |  _/   / _|\__ \ _||   /\ V / _ \| |  | | (_) | .` |
 |_| |_|_\___|___/___|_|_\ \_/_/ \_\_| |___\___/|_|\_|
                                                      

Disclaimer
=======
We handle of lot legacy code that we get sent. By error, sometimes a copyrighted file
that we don't have the permission to release could by mistake be included in the release archives.
If you find something that shouldn't be there, and you can prove that you are the copyright
owner to it, please contact us and we will do our best to resolve the issue.


