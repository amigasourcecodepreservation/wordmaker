/*
* This file is part of WordMaker.
* Copyright (C) 1996 - 2018 Canux Corporation
* 
* WordMaker is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* WordMaker is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with WordMaker.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/* *		WordMaker *		Copyright (c) 1989 New Horizons Software, Inc. * *		Undo handler routines */#include "WordMaker.h"#include "Proto.h"/* *	External variables */extern MenuHandle	menuList[];extern Str255	strBuff;/* *	Local variables and definitions */#define UNDOMODE_NONE	0	/* Types for undoMode */#define UNDOMODE_UNDO	1#define UNDOMODE_REDO	2static WindowPtr	undoWindow;static WORD	undoMode;		/* Inited to UNDOMODE_NONE */static BOOL (*undoRoutine)(WindowPtr, WORD);/* *	Set undo menu item to specified text */void SetUndoMenuItem(WORD undoType){	MenuHandle editMenu;	editMenu = menuList[EDIT_MENU - FIRST_MENU];	GetIndString(strBuff, STRN_UNDO, undoType);	SetItem(editMenu, UNDO_ITEM, strBuff);	if (undoType == UNDO_NONE) {		DisableItem(editMenu, UNDO_ITEM);	}	else {		EnableItem(editMenu, UNDO_ITEM);	}}/* *	Set up undo routine *	Does not set up undo menu item text *	Return success status */BOOL UndoOn(WindowPtr window, BOOL (*routine)()){	WORD item;	BOOL success;	DialogPtr dlog;	if (undoRoutine && undoMode != UNDOMODE_NONE)		(void) (*undoRoutine)(undoWindow, UNDOCMD_CLEAR);	/* Will always succeed */	undoRoutine = routine;	undoMode = UNDOMODE_UNDO;	undoWindow = window;	if ((success = (*undoRoutine)(window, UNDOCMD_INIT)) == FALSE) {		undoRoutine = NULL;		undoMode = UNDOMODE_NONE;		SetUndoMenuItem(UNDO_NONE);		if ((dlog = StartDialog(NOUNDO_DIALOG, TRUE)) == NULL)			Error(ERR_NO_MEM);		else {			SysBeep(5);			ShowWindow(dlog);			do {				ModalDialog(DialogFilter, &item);			} while (item != OK && item != Cancel);			EndDialog(dlog);			if (item == OK)				success = TRUE;		}	}	return (success);}/* *	Clear undo buffer and disable undo */void UndoOff(){	if (undoRoutine && undoMode != UNDOMODE_NONE)		(void) (*undoRoutine)(undoWindow, UNDOCMD_CLEAR);	undoRoutine = NULL;	undoMode = UNDOMODE_NONE;	SetUndoMenuItem(UNDO_NONE);}/* *	Handle undo command *	Does not handle changing undo menu item text */void DoUndo(WindowPtr window){	BOOL success;	if (undoRoutine == NULL || undoMode == UNDOMODE_NONE) {		SysBeep(5);		UndoOff();		return;	}	if (undoMode == UNDOMODE_UNDO) {		success = (*undoRoutine)(window, UNDOCMD_UNDO);		undoMode = UNDOMODE_REDO;	}	else {		success = (*undoRoutine)(window, UNDOCMD_REDO);		undoMode = UNDOMODE_UNDO;	}	if (!success)		Error(ERR_NO_MEM);	SetAllMenus();}