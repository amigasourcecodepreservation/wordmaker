/*
* This file is part of WordMaker.
* Copyright (C) 1996 - 2018 Canux Corporation
* 
* WordMaker is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* WordMaker is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with WordMaker.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/* *		WordMaker *		Copyright (c) 1989 New Horizons Software, Inc. * *		Error report routines */#include <TB_Memory.h>#include "WordMaker.h"#include "Proto.h"/* *	External variables */extern Str255	strBuff;/* *	Local variables *//* *	Error report routine */void Error(register WORD errNum){	register WORD num;	InitCursor();	SetUndoMenuItem(UNDO_NONE);	if (errNum > ERR_MAX_ERROR)		errNum = ERR_UNKNOWN;	GetIndString(strBuff, STRN_ERR, errNum);	ParamText(strBuff, NULL, NULL, NULL);	(void) StopAlert(ERROR_ALERT, NULL);}/* *	Report DOS error */void DOSError(WORD errNum, OSErr osErrNum){	register WORD num;	TextChar err1Text[50], err2Text[50];	InitCursor();	SetUndoMenuItem(UNDO_NONE);	if (errNum > ERR_MAX_ERROR)		errNum = ERR_UNKNOWN;	GetIndString(err1Text, STRN_ERR, errNum);	if (osErrNum == DOSERR_DEMO || osErrNum == DOSERR_BADFILE) {		GetIndString(err2Text, STRN_DOSERR, osErrNum);		ParamText(err1Text, err2Text, NULL, NULL);	}	else		ParamText(err1Text, NULL, NULL, NULL);	(void) StopAlert(ERROR_ALERT, NULL);}/* *	Display "About" dialog */void DoAbout(){	WORD item;	LONG avail;	register DialogPtr dlog;	avail = MemAvail(0);	NumToString(avail/1024, strBuff);	ParamText(strBuff, NULL, NULL, NULL);	if ((dlog = StartDialog(ABOUT_DIALOG, TRUE)) == NULL)		return;	ShowWindow(dlog);	ModalDialog(DialogFilter, &item);	EndDialog(dlog);	SetPointerShape();}/* *	Check remaining memory and give warning if getting low *	Require at least 8K of total free memory, and 1K of contiguous free memory */void CheckMemory(){	LONG total, contig;	register StringHandle strHandle;	PurgeSpace(&total, &contig);	if (total < 0x2000L || contig < 0x400L) {		if (contig < 0x100L)			SysBeep(5);		else {			InitCursor();			strHandle = GetString(STR_LOWMEM);			HLock(strHandle);			ParamText(*strHandle, NULL, NULL, NULL);			(void) CautionAlert(ERROR_ALERT, NULL);			HUnlock(strHandle);		}	}}