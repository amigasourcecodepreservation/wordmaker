/*
* This file is part of WordMaker.
* Copyright (C) 1996 - 2018 Canux Corporation
* 
* WordMaker is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* WordMaker is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with WordMaker.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/* *	Buffered I/O routines *	Copyright (c) 1989 New Horizons Software, Inc. * *	Get routines return byte/nibble or -1 if EOF/error *	MUST call ClearBuff() before using routines, or after file seek */#include "WordMaker.h"#include "Proto.h"extern UBYTE	fileBuff[];static BOOL	needByte;static WORD	buffPtr, byteBuff;static LONG	buffSize;void ClearBuff(){	needByte = TRUE;	buffPtr = buffSize = FILEBUFF_SIZE;}WORD GetByte(File file){	OSErr err;	if (buffPtr == buffSize) {		buffSize = FILEBUFF_SIZE;		if ((err = FSRead(file, &buffSize, (Ptr) fileBuff)) != noErr &&			(err != eofErr || buffSize == 0))			return (-1);		buffPtr = 0;	}	return ((WORD) fileBuff[buffPtr++]);}#define NEXT_BYTE(file)	((buffPtr == buffSize) ? GetByte(file) : fileBuff[buffPtr++])WORD GetNibble(File file){	if (needByte) {		if ((byteBuff = NEXT_BYTE(file)) == -1)			return (-1);		needByte = FALSE;		return ((WORD) ((byteBuff >> 4) & 0x0F));	}	needByte = TRUE;	return ((WORD) (byteBuff & 0x0F));}