/*
* This file is part of WordMaker.
* Copyright (C) 1996 - 2018 Canux Corporation
* 
* WordMaker is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* WordMaker is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with WordMaker.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/* *		WordMaker *		Copyright (c) 1989 New Horizons Software, Inc. * *		Structures used for MacWrite file load and save */typedef struct {	WORD	Version;	WORD	NumDocParas, NumHeadParas, NumFootParas;	BYTE	TitlePage, pad1;	BYTE	ShowClip, ShowFoot, ShowHead, HideRulers;	WORD	ActiveWindow;	WORD	StartPage;	LONG	FreeList;	WORD	FreeListLen, FreeListAlloc;	WORD	pad2[7];	TPrint	PrintRec;} MWGlobals;typedef struct {	WORD	SelStartPara, SelStartLoc, SelEndPara, SelEndLoc;	WORD	VertOffset;	WORD	NeedRedraw;	LONG	InfoArrayPos;	WORD	InfoArrayLen;	LONG	LineHeights;	WORD	LineHeightLen;	WORD	PageNumTop, PageNumLeft, DateTop, DateLeft, TimeTop, TimeLeft;	LONG	pad;	BYTE	OvalRedraw, OvalUpdate;	WORD	ActiveStyle, ActiveFont;} MWWindVars;typedef struct {	WORD	NumBytes;/*	BYTE	Data[];	*/} MWLineHeights;typedef struct {	WORD	ParaHeight, ParaPos;	BYTE	ParaPage, ParaHandle[3];	LONG	DataPos;					/* High byte is status byte */	WORD	DataLen, Formats;} MWInfoArray;typedef struct {	LONG	Position, Size;} MWFreeList;typedef struct {	WORD	Len;/*	BYTE	Data[];	*/} MWParaData;typedef struct {	WORD	Loc;	BYTE	Size, Style;	WORD	FontNum;} MWFormatRun;typedef struct {	WORD	LeftMargin, RightMargin;	BYTE	Justify, NumTabs;	WORD	Spacing;	WORD	LeftIndent;	WORD	TabArray[10];	LONG	pad;} MWRulerData;typedef struct {	Rect	Rect;/*	Picture	Pict;	*/} MWPictureData;#define ON	0xFF#define OFF	0x00#define MACWRITE_VERSION	6