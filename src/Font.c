/*
* This file is part of WordMaker.
* Copyright (C) 1996 - 2018 Canux Corporation
* 
* WordMaker is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* WordMaker is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with WordMaker.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/* *		WordMaker *		Copyright (c) 1989 New Horizons Software, Inc. * *		Font number translation routines (for file save and load) */#include <Script.h>#include <TB_Memory.h>#include "WordMaker.h"#include "Proto.h"/* *	External variables */extern Str255	strBuff;/* *	Local variables and definitions */typedef struct _FontEntry {	struct _FontEntry	*Next;	UWORD				OldFontNum, NewFontNum;} FontEntry, *FontEntryPtr;static FontEntry	*fontTable;		/* Pointer to first entry in table */									/* Inited to NULL *//* *	Get font number of font with given name *	fontName is a NULL terminated string *	Return FALSE if no such font found (and set fontNum to default app font) */BOOL GetFontNum(UBYTE *fontName, register UWORD *fontNum){	CtoPstr((char *) fontName);	GetFNum(fontName, fontNum);	if (*fontNum == 0) {		GetFontName(0, strBuff);		if (!EqualString(fontName, strBuff, FALSE, FALSE)) {			*fontNum = GetAppFont();			return (FALSE);		}	}	return (TRUE);}/* *	Add font entry to font table *	Do not add if entry for given oldFontNum already exists *	Return FALSE if not enough memory */BOOL AddFontEntry(UWORD oldFontNum, UWORD newFontNum){	register FontEntryPtr newEntry, prevEntry;	prevEntry = NULL;	for (newEntry = fontTable; newEntry; newEntry = newEntry->Next) {		if (newEntry->OldFontNum == oldFontNum)			return (TRUE);		prevEntry = newEntry;	}	if ((newEntry = MemAlloc(sizeof(FontEntry), 0)) == NULL)		return (FALSE);	newEntry->Next = NULL;	newEntry->OldFontNum = oldFontNum;	newEntry->NewFontNum = newFontNum;	if (prevEntry)		prevEntry->Next = newEntry;	else		fontTable = newEntry;	return (TRUE);}/* *	Get new font number from old font number *	Return FALSE if entry not found (and set to default app font) */BOOL GetFontEntry(UWORD oldFontNum, UWORD *newFontNum){	register FontEntryPtr fontEntry;	for (fontEntry = fontTable; fontEntry; fontEntry = fontEntry->Next) {		if (fontEntry->OldFontNum == oldFontNum) {			*newFontNum = fontEntry->NewFontNum;			return (TRUE);		}	}	*newFontNum = GetAppFont();	return (FALSE);}/* *	Get font number of given table entry number *	Return FALSE if no such item */BOOL FontTableItem(UWORD itemNum, UWORD *oldFontNum){	register FontEntry *fontEntry;	fontEntry = fontTable;	while (fontEntry && itemNum--)		fontEntry = fontEntry->Next;	if (fontEntry) {		*oldFontNum = fontEntry->OldFontNum;		return (TRUE);	}	return (FALSE);}/* *	Dispose of all table entries */void DisposeFontTable(){	register FontEntryPtr fontEntry, nextEntry;	for (fontEntry = fontTable; fontEntry; fontEntry = nextEntry) {		nextEntry = fontEntry->Next;		MemFree(fontEntry, sizeof(FontEntry));	}	fontTable = NULL;}